# podfeed

This repo contains scripts for generating a local podcast feed.

Requirements:

* a Linux web server
* a directory tree containing the mp3 files to distribute
* perl Config::IniFiles;
* perl XML::RSS
